package com.ds.MyHashMap;

public class MyHashMap<K,V> {

	private final int size = 5;
	private Entry<K,V>[] HashArray;
	
	//Array initialization
	
	public MyHashMap() {
		
		HashArray = new Entry [size];
	}
	

	
	public void put(K Key, V Value) {
		
		int hash = Key.hashCode() % size;
		Entry<K,V> check = HashArray[hash];
		
		if(check == null) {
			HashArray[hash]= new Entry<K,V>(Key, Value);
		}else {
			while(check.Next != null) {
				if(check.getKey() == Key) {
					check.setValue(Value);
					return;
				}
				check = check.Next;
			}
			if(check.getKey() == Key) {
				check.setValue(Value);
				return;
			}
			
			check.Next = new Entry<K,V>(Key , Value);
		}
		
	}
	
	public V get(K Key) {
		int hash = Key.hashCode() % size;
		Entry<K,V> check = HashArray[hash];
		if(check ==null) {
			return null;
		}
		
		while(check != null) {
			if(check.getKey()==Key) {
				return check.getValue();
			}
			check = check.Next;
		}
		return null;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < size; i++) {
			if(HashArray[i] != null) {
				sb.append("Index "+ i + " " + "Key " + HashArray[i] + "\n");
			}else {
				sb.append(i + " " + "null" + "\n");
			}
		}
		return sb.toString();
	}
	
	
}
