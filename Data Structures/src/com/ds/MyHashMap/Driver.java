package com.ds.MyHashMap;

public class Driver {

	public static void main(String[] args) {
		
		
		MyHashMap<Integer, String> myHashMap = new MyHashMap<Integer, String>();
		myHashMap.put(1, "Michael");
		myHashMap.put(2, "Agness");
		myHashMap.put(3, "Musonda");
		myHashMap.put(4, "Reynolds");
		myHashMap.put(5, "Nawa");
		
		System.out.println(myHashMap);
		System.out.println(myHashMap.get(1));

	}

}
