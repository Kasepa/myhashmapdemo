package com.ds.MyHashMap;

public class Entry <K,V>{

	private K Key;
	private V Value;
	public Entry <K,V> Next;
	
	public Entry (K Key, V Value) {
		this.Key = Key;
		this.Value = Value;
	}
	
	public Entry() {
		this.Next= null;
	}
	
	public K getKey() {
		return this.Key;
	}
	
	public V getValue() {
		return this.Value;
	}
	
	public void setValue(V Value) {
		this.Value = Value;
	}
	
	@Override
	public String toString() {
		Entry<K,V> current = this;
		StringBuilder sb = new StringBuilder();
		while(current != null) {
			sb.append(current.Key +  " Value " + "-> " + current.Value + ",");
			current = current.Next;
		}
		return sb.toString();
		
	}
}

